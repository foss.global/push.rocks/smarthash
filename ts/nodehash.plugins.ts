// node native scope
import crypto from 'crypto';
import * as fs from 'fs';
import * as path from 'path';
import * as stream from 'stream';

export { crypto, fs, path, stream };

// pushrocks scope
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartjson from '@pushrocks/smartjson';

export { smartpromise, smartjson };

// thirdparty scope
import through2 from 'through2';

export { through2 };
