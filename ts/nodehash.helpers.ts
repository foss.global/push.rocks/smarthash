import * as plugins from './nodehash.plugins.js';

export const hashStreamPipeStop = (resolveFunctionArg: (inputArg: string) => any) => {
  const forEach = (chunk: Buffer, enc: any, cb: any) => {
    resolveFunctionArg(chunk.toString('utf8'));
    cb(null, chunk);
  };

  const atEnd = (cb: any) => {
    cb();
  };

  return plugins.through2(forEach, atEnd);
};
